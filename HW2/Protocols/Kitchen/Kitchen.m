//
//  Kitchen.m
//  HW2
//
//  Created by Nazar Starantsov on 28/09/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Kitchen.h"

@implementation Kitchen

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.isCooking = NO;
    }
    
    return self;
}

- (void)startCooking: (NSString*) meal
{
    self.isCooking = YES;
    
    for (NSInteger i = 0; i < 5; i++)
    {
        NSLog(@"Кухня готовит: %@...",meal);
    }
    self.isCooking = NO;
    // TODO: сделать проверку делегата на реализованность данного метода @select.....
    [self.delegate kitchenDidFinishCooking];
}

@end

