//
//  KitchenProtocol.h
//  HW2
//
//  Created by Nazar Starantsov on 28/09/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//


#import <UIKit/UIKit.h>

/**
 Протокол работы кухни
 */
@protocol KitchenProtocol <NSObject>

/**
 Уведомляет о том, кухня приготовила блюдо
 */
- (void)kitchenDidFinishCooking;

@end
