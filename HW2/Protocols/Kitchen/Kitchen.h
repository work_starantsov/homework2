//
//  Kitchen.h
//  HW2
//
//  Created by Nazar Starantsov on 28/09/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KitchenProtocol.h"

@interface Kitchen : NSObject

@property (nonatomic, weak) id <KitchenProtocol> delegate;

@property (nonatomic, assign) BOOL isCooking;

/**
Кухня начала готовить блюдо
*/
- (void)startCooking: (NSString*) meal;

@end
