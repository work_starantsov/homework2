//
//  Waiter.h
//  HW2
//
//  Created by Nazar Starantsov on 28/09/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "WaiterProtocol.h"
#import "Kitchen.h"

@interface Waiter : NSObject

- (instancetype)init: (Kitchen *)kitchen;

@property (nonatomic, weak) id <WaiterProtocol> delegate;
@property (nonatomic, assign) BOOL isBusyDelivering;

@end
