//
//  Waiter.m
//  HW2
//
//  Created by Nazar Starantsov on 28/09/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KitchenProtocol.h"
#import "Waiter.h"
#import "Kitchen.h"
#import "GuestProtocol.h"

@interface Waiter () <GuestProtocol, KitchenProtocol>

@property (nonatomic, strong) Kitchen *kitchen;

@end

@implementation Waiter

- (instancetype)init: (Kitchen *)kitchen
{
    self = [super init];
    if (self) {
        self.kitchen = kitchen;
        self.kitchen.delegate = self;
        self.isBusyDelivering = NO;
    }
    
    return self;
}

- (void)guestDidTellWhatHeWants:(NSString *)meal
{
    // Поговорив с клиентом и записав его заказ передать заказ кухне
    self.isBusyDelivering = YES;
    for (NSInteger i = 0; i < 2; i++)
    {
        NSLog(@"Официант записал заказ клиента и идет на кухню...");
    }
    self.isBusyDelivering = NO;
    
    [self.kitchen startCooking: meal];
}

- (void)kitchenDidFinishCooking
{
    // Забрать заказ и передать клиенту
    self.isBusyDelivering = YES;
    
    for (NSInteger i = 0; i < 5; i++)
    {
        NSLog(@"Официант забирает блюдо с кухни и несет его клиенту...");
    }
    self.isBusyDelivering = NO;
    
    // TODO: сделать проверку делегата на реализованность данного метода @select.....
    [self.delegate waiterDidDeliverOrderAndNotifiedGuest];
}

@end
