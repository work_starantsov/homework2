//
//  WaiterProtocol.h
//  HW2
//
//  Created by Nazar Starantsov on 28/09/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

/**
Протокол работы  официанта
*/
@protocol WaiterProtocol <NSObject>

/**
   Уведомляет о том, что официант забрал заказ из кухни и принес его гостю
*/
- (void)waiterDidDeliverOrderAndNotifiedGuest;

@end
