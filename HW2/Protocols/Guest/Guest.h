//
//  Guest.h
//  HW2
//
//  Created by Nazar Starantsov on 28/09/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Waiter.h"
#import "GuestProtocol.h"

@interface Guest : NSObject

- (instancetype)init: (Waiter *)waiter;

@property (nonatomic, weak) id <GuestProtocol> delegate;

@property (nonatomic, assign) BOOL isWaitingOrder;

- (void) makeOrder;

@end
