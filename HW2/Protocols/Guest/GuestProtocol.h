//
//  GuestProtocol.h
//  HW2
//
//  Created by Nazar Starantsov on 28/09/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

/**
Протокол Гостя (общение с официантом)
*/
@protocol GuestProtocol <NSObject>

/**
   Уведомляет о том, что гость определился с покупкой и готов делать заказ блюд
*/
- (void) guestDidTellWhatHeWants: (NSString*) meal;

@end
