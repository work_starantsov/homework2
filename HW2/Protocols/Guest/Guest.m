//
//  Guest.m
//  HW2
//
//  Created by Nazar Starantsov on 28/09/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Guest.h"
#import "WaiterProtocol.h"
#import "Waiter.h"

@interface Guest () <WaiterProtocol>

@property (nonatomic, strong) Waiter *waiter;
- (void) takeCookedFoodAndStartEating;

@end

@implementation Guest

- (instancetype)init: (Waiter *)waiter
{
    self = [super init];
    if (self) {
        self.waiter = waiter;
        self.waiter.delegate = self;
        
        self.delegate = self.waiter;
        self.isWaitingOrder = NO;
    }
    
    return self;
}

- (void) makeOrder
{
    // Клиент думает что заказать и когда решает дергает Официанта
    // Вызывая методы делегата, которые Официант реализует
    NSLog(@"Клиент смотрит меню и решает что хочет купить");
    [self.delegate guestDidTellWhatHeWants:@"Мисо-суп, роллы 'Филадельфия'"];
}

- (void)waiterDidDeliverOrderAndNotifiedGuest
{
    self.isWaitingOrder = NO;
    [self takeCookedFoodAndStartEating];
}

- (void) takeCookedFoodAndStartEating
{
    NSLog(@"Клиент съел блюдо!");
}

@end
