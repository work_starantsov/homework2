//
//  ViewController.m
//  HW2
//
//  Created by Nazar Starantsov on 28/09/2019.
//  Copyright © 2019 Nazar Starantsov. All rights reserved.
//

#import "ViewController.h"
#import "Kitchen.h"
#import "Waiter.h"
#import "Guest.h"

@interface ViewController ()

@property (nonatomic, strong) Kitchen *kitchen;
@property (nonatomic, strong) Waiter *waiter;
@property (nonatomic, strong) Guest *guest;

@end

@implementation ViewController

- (void)viewDidLoad
{
    // В консоли можно увидеть логи, которые показывают как работает программа :) :) :)
    
    // Кухня работает сама по себе
    self.kitchen = [[Kitchen alloc] init];
    
    // Официанту должна быть присвоена кухня. Иначе официант он не официант, а безработный :)
    self.waiter = [[Waiter alloc] init: self.kitchen];
    
    // Гостю положен официант
    self.guest = [[Guest alloc] init: self.waiter];
    
    [self.guest makeOrder];
    
    [super viewDidLoad];
}



@end
